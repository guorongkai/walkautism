using UnityEngine;
using System.Collections;

/// <summary>
/// CoroutineActions class is used to simulate simple animations with moving, scaling and lerping of colors
/// </summary>
public class CoroutineActions : MonoBehaviour {
	
	
	private static CoroutineActions instance = null;
	
	void Start () 
	{
		if(instance == null)
			instance = this;
	}
	
	/// <summary>
	/// Fades the object.
	/// </summary>
	/// <returns>
	/// The object.
	/// </returns>
	/// <param name='objectToFade'>
	/// Object texture.
	/// </param>
	/// <param name='colorToLerpTo'>
	/// Color to lerp to.
	/// </param>
	/// <param name='lerpTime'>
	/// Lerp time.
	/// </param>
	//
	public static IEnumerator LerpColor(GUITexture objectTexture, Color colorToLerpTo, float lerpTime)
	{
		Color originalColor = objectTexture.color;
		
		float counter = 0;
		
		while(counter < 1.0f)
		{
			objectTexture.color = Color.Lerp(originalColor, colorToLerpTo, counter);
			counter += Time.deltaTime / lerpTime;
	
			yield return new WaitForEndOfFrame();
		}
	
		objectTexture.color = colorToLerpTo;
		
		Debug.LogWarning("Lerp Color complete");
	}
	
	/// <summary>
	/// Fades the out object.
	/// </summary>
	/// <returns>
	/// The out object.
	/// </returns>
	/// <param name='objectToFade'>
	/// Object to fade.
	/// </param>
	/// <param name='colorToLerpTo'>
	/// Color to lerp to.
	/// </param>
	/// <param name='lerpTime'>
	/// Lerp time.
	/// </param>
	public static IEnumerator LerpColor(GameObject objectToFade, Color colorToLerpTo, float lerpTime)
	{
		Color originalColor = objectToFade.renderer.material.color;
		
		float counter = 0.0f;
		
		while(counter < 1.0f)
		{
			objectToFade.renderer.material.color = Color.Lerp(originalColor, colorToLerpTo, counter);
			counter += Time.deltaTime / lerpTime;
	
			yield return new WaitForEndOfFrame();
		}
	
		objectToFade.renderer.material.color = colorToLerpTo;
		
		Debug.LogWarning("Lerp Color complete");
	}
	
	/// <summary>
	/// Lerps the object position.
	/// </summary>
	/// <returns>
	/// The object position.
	/// </returns>
	/// <param name='objectToMove'>
	/// Object to move.
	/// </param>
	/// <param name='positionToMoveTo'>
	/// Position to move to.
	/// </param>
	/// <param name='lerpTime'>
	/// Lerp time.
	/// </param>
	public static IEnumerator LerpObjectPosition(GameObject objectToMove, GameObject positionToMoveTo, float lerpTime)
	{
		while(Vector3.Distance(objectToMove.transform.position, positionToMoveTo.transform.position) > 0.125f)
		{
			objectToMove.transform.position = Vector3.Lerp(objectToMove.transform.position, positionToMoveTo.transform.position, Time.deltaTime * lerpTime);

			yield return new WaitForEndOfFrame();

		}
		objectToMove.transform.position = positionToMoveTo.transform.position;
		
		Debug.LogWarning("Lerp Position complete");
		
	}
	
	/// <summary>
	/// Lerps the object scale.
	/// </summary>
	/// <returns>
	/// The object scale.
	/// </returns>
	/// <param name='objectScale'>
	/// Object scale.
	/// </param>
	/// <param name='objectToScaleTo'>
	/// Object to scale to.
	/// </param>
	/// <param name='lerpTime'>
	/// Lerp time.
	/// </param>
	public static IEnumerator LerpObjectScale(GameObject objectScale, Vector3 objectToScaleTo, float lerpTime)
	{
		float counter = 0.0f;
		
		while(counter < 1.0f)
		{
			objectScale.transform.localScale = Vector3.Lerp(objectScale.transform.localScale, objectToScaleTo, lerpTime);
			counter += Time.deltaTime / lerpTime;
			
			yield return new WaitForEndOfFrame();
		}
		objectScale.transform.localScale = objectToScaleTo;
		
		Debug.LogWarning("Lerp Scale Complete");
	}
	
	// Types out text just like a typewriter
	public static IEnumerator TypeWriterText(float typeDelay, string stringToType, GUIText GUIDisplayText)
	{
		foreach(char letter in stringToType.ToCharArray())
		{
			stringToType += letter;
			GUIDisplayText.text = stringToType;
			yield return new WaitForSeconds(typeDelay);
		}
	}
	
	public static CoroutineActions Instance 
	{
		get{ return instance; }	
	}
}
