using UnityEngine;
using System.Collections;

public class MenuManager : MonoBehaviour 
{
#region Public Attributes
	public enum MenuState{ StartMenu, AvatarSelection, MultiplayerLobby, None};
	
	public UIPanel startPanel;
	public UIPanel avatarPanel;
	public UIPanel multiplayerLobbyPanel;
	
	public UIProgressBar menuProgressBar;
	public UIButton avatarNextButton;
	public UIButton startButton;
#endregion
	
#region Private Attributes
	private bool updateProgressBar = false;	
	private float expectedProgressValue;
#endregion
	
	// Use this for initialization
	void Start () 
	{
		startPanel.BringIn();
		
		expectedProgressValue = menuProgressBar.Value + 0.08f;
		updateProgressBar = true;
		
		startButton.AddInputDelegate(StartButtonAction);
		avatarNextButton.AddInputDelegate(AvatarNextButtonAction);
	}
	
	void LateUpdate () 
	{
		UpdateManager();
	}
	
	private void UpdateManager()
	{
		if(updateProgressBar)
		{
			LerpProgressBarValue(expectedProgressValue);	
		}		
	}
	
	private void LerpProgressBarValue(float lerpToValue)
	{
		menuProgressBar.Value = Mathf.Lerp(menuProgressBar.Value, lerpToValue, Time.deltaTime * 5.0f);
		
		if(menuProgressBar.Value >= lerpToValue)
		{
			updateProgressBar = false;
		}	
	}
	
	private void StartButtonAction(ref POINTER_INFO ptr)
	{
		if(ptr.evt == POINTER_INFO.INPUT_EVENT.TAP)
		{
			startPanel.Dismiss();
			avatarPanel.BringIn();
			
			expectedProgressValue = menuProgressBar.Value + 0.45f;
			updateProgressBar = true;
		}
	}
	
	private void AvatarNextButtonAction(ref POINTER_INFO ptr)
	{
		if(ptr.evt == POINTER_INFO.INPUT_EVENT.TAP)
		{
			avatarPanel.Dismiss();
			multiplayerLobbyPanel.BringIn();
			ConnectionManager.Instance.ShowConnectionGUI = true;
			
			expectedProgressValue = menuProgressBar.Value + (1.0f - menuProgressBar.Value);
			updateProgressBar = true;
		}
	}
}
