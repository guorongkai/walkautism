using UnityEngine;
using System.Collections;

public class ConnectionManager : MonoBehaviour 
{
	public GUISkin connectionGUISkin;
	
	private bool connectFailed = false;
	private bool showConnectionGUI = false;
    private string roomName = "myRoom";
	private string playerName = "";
    private Vector2 scrollPos = Vector2.zero;	
	
	private static ConnectionManager sInstance = null;
	
	void Awake()
	{
		if(!sInstance)
			sInstance = this;
		
		PhotonNetwork.automaticallySyncScene = true;

        if (PhotonNetwork.connectionStateDetailed == PeerState.PeerCreated)
        {
            PhotonNetwork.ConnectUsingSettings("1.0");
        }
		
		playerName = "Guest" + Random.Range(1, 999);
	}
	
	void OnGUI()
	{
//        if (!PhotonNetwork.connected)
//        {
//            if (PhotonNetwork.connectionState == ConnectionState.Connecting)
//            {
//                GUILayout.Label("Connecting " + PhotonNetwork.ServerAddress);
//                GUILayout.Label(Time.time.ToString());
//            }
//            else
//            {
//                GUILayout.Label("Not connected. Check console output.");
//            }
//
//            if (this.connectFailed)
//            {
//                GUILayout.Label("Connection failed. Check setup and use Setup Wizard to fix configuration.");
//                GUILayout.Label(String.Format("Server: {0}:{1}", new object[] {PhotonNetwork.ServerAddress, PhotonNetwork.PhotonServerSettings.ServerPort}));
//                GUILayout.Label("AppId: " + PhotonNetwork.PhotonServerSettings.AppID);
//                
//                if (GUILayout.Button("Try Again", GUILayout.Width(100)))
//                {
//                    this.connectFailed = false;
//                    PhotonNetwork.ConnectUsingSettings("1.0");
//                }
//            }
//
//            return;
//        }		
		//if(showConnectionGUI)
		if(showConnectionGUI)
			UpdateConnectionGUI();
	}
	
	private void UpdateConnectionGUI()
	{
		GUI.skin = connectionGUISkin;
		
        GUI.Box(new Rect((Screen.width - 400) / 2, (Screen.height * 0.6f) / 2, 400, 300), "Multiplayer Lobby!");
        GUILayout.BeginArea(new Rect((Screen.width - 400) / 2, (Screen.height * 0.6f) / 2, 400, 300));

        GUILayout.Space(25);

        // Player name
        GUILayout.BeginHorizontal();
		
        GUILayout.Label("Player name:", GUILayout.Width(100));
        playerName = GUILayout.TextField(playerName);
		PhotonNetwork.playerName = playerName;
        GUILayout.Space(105);
  
        GUILayout.EndHorizontal();

        GUILayout.Space(15);

        // Join room by title
        GUILayout.BeginHorizontal();
        GUILayout.Label("Room name:", GUILayout.Width(100));
        roomName = GUILayout.TextField(roomName);
        
        if(GUILayout.Button("Create Room", GUILayout.Width(100)))
        {
            PhotonNetwork.CreateRoom(roomName, true, true, 10);
        }

        GUILayout.EndHorizontal();

        // Create a room (fails if exist!)
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
 
        if (GUILayout.Button("Join Room", GUILayout.Width(100)))
        {
            PhotonNetwork.JoinRoom(roomName);
        }

        GUILayout.EndHorizontal();


        GUILayout.Space(15);

        // Join random room
        GUILayout.BeginHorizontal();

        GUILayout.Label(PhotonNetwork.countOfPlayers + " users are online in " + PhotonNetwork.countOfRooms + " rooms.");
        GUILayout.FlexibleSpace();
        if (GUILayout.Button("Join Random", GUILayout.Width(100)))
		{
            PhotonNetwork.JoinRandomRoom();
        }
        

        GUILayout.EndHorizontal();

        GUILayout.Space(15);
        if (PhotonNetwork.GetRoomList().Length == 0)
        {
            GUILayout.Label("Currently no games are available.");
            GUILayout.Label("Rooms will be listed here, when they become available.");
        }
        else
        {
            GUILayout.Label(PhotonNetwork.GetRoomList() + " currently available. Join either:");

            // Room listing: simply call GetRoomList: no need to fetch/poll whatever!
            scrollPos = GUILayout.BeginScrollView(scrollPos);
            foreach (RoomInfo roomInfo in PhotonNetwork.GetRoomList())
            {
                GUILayout.BeginHorizontal();
                GUILayout.Label(roomInfo.name + " " + roomInfo.playerCount + "/" + roomInfo.maxPlayers);
                if (GUILayout.Button("Join"))
                {
                    PhotonNetwork.JoinRoom(roomInfo.name);
                }

                GUILayout.EndHorizontal();
            }

            GUILayout.EndScrollView();
        }

        GUILayout.EndArea();		
	}
	
    public void OnJoinedRoom()
    {
        Debug.Log("OnJoinedRoom");
    }

    public void OnCreatedRoom()
    {
        Debug.Log("OnCreatedRoom");
        PhotonNetwork.LoadLevel("ATTCenterTrack");
    }

    public void OnDisconnectedFromPhoton()
    {
        Debug.Log("Disconnected from Photon.");
    }	
	
    public void OnFailedToConnectToPhoton(object parameters)
    {
        connectFailed = true;
		
        Debug.Log("OnFailedToConnectToPhoton. StatusCode: " + parameters);
    }
	
#region Public Accessors	
	public bool ShowConnectionGUI
	{
		get{ return showConnectionGUI; }
		set{ showConnectionGUI = value; }
	}
	
	public static ConnectionManager Instance
	{
		get{ return sInstance; }	
	}
	
#endregion
}
