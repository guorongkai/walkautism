info face="Mensch" size=24 bold=0 italic=0 charset="" unicode=1 stretchH=100 smooth=1 aa=1 padding=0,0,0,0 spacing=1,1 outline=0
common lineHeight=24 base=20 scaleW=512 scaleH=512 pages=1 packed=0 alphaChnl=0 redChnl=4 greenChnl=4 blueChnl=4
page id=0 file="MENSCH_24_0.png"
chars count=96
char id=32   x=406   y=20    width=3     height=1     xoffset=-1    yoffset=23    xadvance=12    page=0  chnl=15
char id=33   x=244   y=20    width=4     height=18    xoffset=0     yoffset=2     xadvance=4     page=0  chnl=15
char id=34   x=323   y=20    width=8     height=6     xoffset=-1    yoffset=2     xadvance=8     page=0  chnl=15
char id=35   x=402   y=20    width=3     height=1     xoffset=-1    yoffset=23    xadvance=20    page=0  chnl=15
char id=36   x=398   y=20    width=3     height=1     xoffset=-1    yoffset=23    xadvance=20    page=0  chnl=15
char id=37   x=394   y=20    width=3     height=1     xoffset=-1    yoffset=23    xadvance=20    page=0  chnl=15
char id=38   x=382   y=20    width=3     height=1     xoffset=-1    yoffset=23    xadvance=20    page=0  chnl=15
char id=39   x=346   y=20    width=4     height=6     xoffset=0     yoffset=1     xadvance=4     page=0  chnl=15
char id=40   x=370   y=20    width=3     height=1     xoffset=-1    yoffset=23    xadvance=7     page=0  chnl=15
char id=41   x=366   y=20    width=3     height=1     xoffset=-1    yoffset=23    xadvance=7     page=0  chnl=15
char id=42   x=312   y=20    width=10    height=8     xoffset=-1    yoffset=2     xadvance=8     page=0  chnl=15
char id=43   x=278   y=20    width=12    height=11    xoffset=0     yoffset=7     xadvance=12    page=0  chnl=15
char id=44   x=339   y=20    width=6     height=6     xoffset=-1    yoffset=17    xadvance=4     page=0  chnl=15
char id=45   x=351   y=20    width=9     height=3     xoffset=5     yoffset=9     xadvance=20    page=0  chnl=15
char id=46   x=361   y=20    width=4     height=3     xoffset=-1    yoffset=17    xadvance=2     page=0  chnl=15
char id=47   x=226   y=20    width=7     height=18    xoffset=0     yoffset=2     xadvance=7     page=0  chnl=15
char id=48   x=167   y=0     width=10    height=20    xoffset=0     yoffset=1     xadvance=10    page=0  chnl=15
char id=49   x=386   y=0     width=10    height=19    xoffset=0     yoffset=1     xadvance=10    page=0  chnl=15
char id=50   x=50    y=0     width=11    height=20    xoffset=0     yoffset=1     xadvance=11    page=0  chnl=15
char id=51   x=86    y=0     width=11    height=20    xoffset=0     yoffset=1     xadvance=11    page=0  chnl=15
char id=52   x=0     y=21    width=11    height=18    xoffset=0     yoffset=2     xadvance=11    page=0  chnl=15
char id=53   x=98    y=0     width=11    height=20    xoffset=0     yoffset=1     xadvance=12    page=0  chnl=15
char id=54   x=239   y=0     width=13    height=19    xoffset=-1    yoffset=2     xadvance=11    page=0  chnl=15
char id=55   x=499   y=0     width=12    height=18    xoffset=-1    yoffset=2     xadvance=10    page=0  chnl=15
char id=56   x=14    y=0     width=11    height=20    xoffset=-1    yoffset=1     xadvance=9     page=0  chnl=15
char id=57   x=253   y=0     width=12    height=19    xoffset=-1    yoffset=1     xadvance=10    page=0  chnl=15
char id=58   x=291   y=20    width=5     height=10    xoffset=0     yoffset=7     xadvance=5     page=0  chnl=15
char id=59   x=271   y=20    width=6     height=14    xoffset=-1    yoffset=7     xadvance=5     page=0  chnl=15
char id=60   x=249   y=20    width=10    height=14    xoffset=0     yoffset=4     xadvance=9     page=0  chnl=15
char id=61   x=297   y=20    width=14    height=8     xoffset=-1    yoffset=7     xadvance=13    page=0  chnl=15
char id=62   x=260   y=20    width=10    height=14    xoffset=0     yoffset=4     xadvance=10    page=0  chnl=15
char id=63   x=0     y=0     width=13    height=20    xoffset=0     yoffset=1     xadvance=13    page=0  chnl=15
char id=64   x=410   y=20    width=3     height=1     xoffset=-1    yoffset=23    xadvance=20    page=0  chnl=15
char id=65   x=397   y=0     width=10    height=19    xoffset=0     yoffset=2     xadvance=10    page=0  chnl=15
char id=66   x=12    y=21    width=11    height=18    xoffset=1     yoffset=2     xadvance=12    page=0  chnl=15
char id=67   x=178   y=0     width=10    height=20    xoffset=1     yoffset=1     xadvance=12    page=0  chnl=15
char id=68   x=24    y=21    width=11    height=18    xoffset=0     yoffset=2     xadvance=11    page=0  chnl=15
char id=69   x=215   y=20    width=10    height=18    xoffset=1     yoffset=2     xadvance=11    page=0  chnl=15
char id=70   x=36    y=21    width=11    height=18    xoffset=0     yoffset=2     xadvance=11    page=0  chnl=15
char id=71   x=122   y=0     width=11    height=20    xoffset=0     yoffset=1     xadvance=11    page=0  chnl=15
char id=72   x=48    y=21    width=11    height=18    xoffset=1     yoffset=2     xadvance=12    page=0  chnl=15
char id=73   x=239   y=20    width=4     height=18    xoffset=1     yoffset=2     xadvance=6     page=0  chnl=15
char id=74   x=156   y=0     width=10    height=20    xoffset=1     yoffset=1     xadvance=12    page=0  chnl=15
char id=75   x=279   y=0     width=12    height=19    xoffset=0     yoffset=1     xadvance=12    page=0  chnl=15
char id=76   x=60    y=21    width=11    height=18    xoffset=1     yoffset=2     xadvance=12    page=0  chnl=15
char id=77   x=419   y=0     width=13    height=18    xoffset=0     yoffset=2     xadvance=14    page=0  chnl=15
char id=78   x=486   y=0     width=12    height=18    xoffset=0     yoffset=2     xadvance=12    page=0  chnl=15
char id=79   x=74    y=0     width=11    height=20    xoffset=0     yoffset=1     xadvance=11    page=0  chnl=15
char id=80   x=72    y=21    width=11    height=18    xoffset=0     yoffset=2     xadvance=11    page=0  chnl=15
char id=81   x=200   y=0     width=10    height=20    xoffset=1     yoffset=1     xadvance=13    page=0  chnl=15
char id=82   x=473   y=0     width=12    height=18    xoffset=0     yoffset=2     xadvance=12    page=0  chnl=15
char id=83   x=26    y=0     width=11    height=20    xoffset=0     yoffset=1     xadvance=12    page=0  chnl=15
char id=84   x=84    y=21    width=11    height=18    xoffset=0     yoffset=2     xadvance=11    page=0  chnl=15
char id=85   x=375   y=0     width=10    height=19    xoffset=1     yoffset=2     xadvance=13    page=0  chnl=15
char id=86   x=352   y=0     width=11    height=19    xoffset=0     yoffset=1     xadvance=11    page=0  chnl=15
char id=87   x=225   y=0     width=13    height=19    xoffset=1     yoffset=1     xadvance=15    page=0  chnl=15
char id=88   x=340   y=0     width=11    height=19    xoffset=0     yoffset=1     xadvance=11    page=0  chnl=15
char id=89   x=328   y=0     width=11    height=19    xoffset=0     yoffset=1     xadvance=11    page=0  chnl=15
char id=90   x=96    y=21    width=11    height=18    xoffset=0     yoffset=2     xadvance=11    page=0  chnl=15
char id=91   x=414   y=20    width=3     height=1     xoffset=-1    yoffset=23    xadvance=20    page=0  chnl=15
char id=92   x=374   y=20    width=3     height=1     xoffset=-1    yoffset=23    xadvance=20    page=0  chnl=15
char id=93   x=378   y=20    width=3     height=1     xoffset=-1    yoffset=23    xadvance=20    page=0  chnl=15
char id=94   x=386   y=20    width=3     height=1     xoffset=-1    yoffset=23    xadvance=20    page=0  chnl=15
char id=95   x=390   y=20    width=3     height=1     xoffset=-1    yoffset=23    xadvance=20    page=0  chnl=15
char id=96   x=332   y=20    width=6     height=6     xoffset=1     yoffset=2     xadvance=8     page=0  chnl=15
char id=97   x=364   y=0     width=10    height=19    xoffset=0     yoffset=2     xadvance=10    page=0  chnl=15
char id=98   x=108   y=21    width=11    height=18    xoffset=1     yoffset=2     xadvance=12    page=0  chnl=15
char id=99   x=145   y=0     width=10    height=20    xoffset=1     yoffset=1     xadvance=12    page=0  chnl=15
char id=100  x=120   y=21    width=11    height=18    xoffset=0     yoffset=2     xadvance=11    page=0  chnl=15
char id=101  x=204   y=21    width=10    height=18    xoffset=1     yoffset=2     xadvance=11    page=0  chnl=15
char id=102  x=132   y=21    width=11    height=18    xoffset=0     yoffset=2     xadvance=11    page=0  chnl=15
char id=103  x=62    y=0     width=11    height=20    xoffset=0     yoffset=1     xadvance=11    page=0  chnl=15
char id=104  x=144   y=21    width=11    height=18    xoffset=1     yoffset=2     xadvance=12    page=0  chnl=15
char id=105  x=234   y=20    width=4     height=18    xoffset=1     yoffset=2     xadvance=6     page=0  chnl=15
char id=106  x=134   y=0     width=10    height=20    xoffset=1     yoffset=1     xadvance=12    page=0  chnl=15
char id=107  x=266   y=0     width=12    height=19    xoffset=0     yoffset=1     xadvance=12    page=0  chnl=15
char id=108  x=156   y=21    width=11    height=18    xoffset=1     yoffset=2     xadvance=12    page=0  chnl=15
char id=109  x=433   y=0     width=13    height=18    xoffset=0     yoffset=2     xadvance=14    page=0  chnl=15
char id=110  x=460   y=0     width=12    height=18    xoffset=0     yoffset=2     xadvance=12    page=0  chnl=15
char id=111  x=110   y=0     width=11    height=20    xoffset=0     yoffset=1     xadvance=11    page=0  chnl=15
char id=112  x=168   y=21    width=11    height=18    xoffset=0     yoffset=2     xadvance=11    page=0  chnl=15
char id=113  x=189   y=0     width=10    height=20    xoffset=1     yoffset=1     xadvance=13    page=0  chnl=15
char id=114  x=447   y=0     width=12    height=18    xoffset=0     yoffset=2     xadvance=12    page=0  chnl=15
char id=115  x=38    y=0     width=11    height=20    xoffset=0     yoffset=1     xadvance=12    page=0  chnl=15
char id=116  x=180   y=21    width=11    height=18    xoffset=0     yoffset=2     xadvance=11    page=0  chnl=15
char id=117  x=408   y=0     width=10    height=19    xoffset=1     yoffset=2     xadvance=13    page=0  chnl=15
char id=118  x=316   y=0     width=11    height=19    xoffset=0     yoffset=1     xadvance=11    page=0  chnl=15
char id=119  x=211   y=0     width=13    height=19    xoffset=1     yoffset=1     xadvance=15    page=0  chnl=15
char id=120  x=304   y=0     width=11    height=19    xoffset=0     yoffset=1     xadvance=11    page=0  chnl=15
char id=121  x=292   y=0     width=11    height=19    xoffset=0     yoffset=1     xadvance=11    page=0  chnl=15
char id=122  x=192   y=21    width=11    height=18    xoffset=0     yoffset=2     xadvance=11    page=0  chnl=15
char id=123  x=418   y=20    width=3     height=1     xoffset=-1    yoffset=23    xadvance=20    page=0  chnl=15
char id=124  x=422   y=19    width=3     height=1     xoffset=-1    yoffset=23    xadvance=20    page=0  chnl=15
char id=125  x=426   y=19    width=3     height=1     xoffset=-1    yoffset=23    xadvance=20    page=0  chnl=15
char id=126  x=430   y=19    width=3     height=1     xoffset=-1    yoffset=23    xadvance=20    page=0  chnl=15
char id=160  x=434   y=19    width=3     height=1     xoffset=-1    yoffset=23    xadvance=12    page=0  chnl=15
kernings count=10
kerning first=32  second=82  amount=1   
kerning first=46  second=69  amount=1   
kerning first=74  second=69  amount=-1  
kerning first=76  second=79  amount=-1  
kerning first=76  second=89  amount=-3  
kerning first=78  second=68  amount=-1  
kerning first=89  second=65  amount=-1  
kerning first=83  second=68  amount=-1  
kerning first=79  second=86  amount=-1  
kerning first=83  second=74  amount=-1  
