using UnityEngine;
using System.Collections;

public class AvatarMenuManager : MonoBehaviour {
	
	private enum AvatarGenderState{ None, Male, Female };
	
	public GUIButton arrowUpButton;
	public GUIButton arrowDownButton;
	public GUIButton selectButton;
	public GUIButton toggleGenderButton;
	
	public GameObject[] maleAvatars;
	public GameObject[] femaleAvatars;
	
	private GameObject curAvatarDisplayedRef; 
	private AvatarGenderState currentGenderState = AvatarGenderState.Male;
	private Vector3 avatarPosition = Vector3.zero;
	
	private int index = 0;
	private int selectedIndex = 0;

	void Awake()
	{
		//GUIButton Delegates
		arrowUpButton.OnButtonClick += OnArrowUpClick;
		arrowDownButton.OnButtonClick += OnArrowDownClick;
		selectButton.OnButtonClick += OnSelectionClick;
		toggleGenderButton.OnButtonClick += OnToggleGenderClick;
	}
	
	void Start () 
	{	
		avatarPosition = new Vector3(this.transform.position.x, this.transform.position.y - 200.0f, this.transform.position.z - 25.0f);
				
		curAvatarDisplayedRef = Instantiate(maleAvatars[index], avatarPosition, maleAvatars[index].transform.rotation) as GameObject;
		curAvatarDisplayedRef.transform.parent = this.gameObject.transform;
	}
	
	void LateUpdate () 
	{
		// Late Update 
	}
	
	void OnDestroy()
	{
		SaveAvatarSelection();
	}
	
#region Private Methods	
	private void SaveAvatarSelection()
	{
		// Saving the current index of the avatar that the user has selected
		PlayerPrefs.SetInt("avatarSelection", selectedIndex);
		PlayerPrefs.SetInt("genderState", (int)currentGenderState);
	}
#endregion
	
#region Button Delegates
	private void OnArrowUpClick()
	{		
		if(curAvatarDisplayedRef != null)
		{
			avatarPosition = new Vector3(this.transform.position.x, this.transform.position.y - 200.0f, this.transform.position.z - 25.0f);
			Destroy(curAvatarDisplayedRef);
			
			if(currentGenderState == AvatarGenderState.Male)
			{
				if(index >= maleAvatars.Length - 1)
					index = 0;
				else
					index++;
				
				curAvatarDisplayedRef = Instantiate(maleAvatars[index], avatarPosition, maleAvatars[index].transform.rotation) as GameObject;
			}
			else
			{
				if(index >= femaleAvatars.Length - 1)
					index = 0;
				else
					index++;			
				
				curAvatarDisplayedRef = Instantiate(femaleAvatars[index], avatarPosition, femaleAvatars[index].transform.rotation) as GameObject;
			}
			
			curAvatarDisplayedRef.transform.parent = this.gameObject.transform;
		}
		
	}
	
	private void OnArrowDownClick()
	{
		if(curAvatarDisplayedRef != null)
		{
			avatarPosition = new Vector3(this.transform.position.x, this.transform.position.y - 200.0f, this.transform.position.z - 25.0f);
			Destroy(curAvatarDisplayedRef);
			
			if(currentGenderState == AvatarGenderState.Male)
			{
				if(index <= 0)
					index = maleAvatars.Length - 1;
				else
					index--;
				
				curAvatarDisplayedRef = Instantiate(maleAvatars[index], avatarPosition, maleAvatars[index].transform.rotation) as GameObject;
			}
			else // We are female...
			{
				if(index <= 0)
					index = femaleAvatars.Length - 1;
				else
					index--;
				
				curAvatarDisplayedRef = Instantiate(femaleAvatars[index], avatarPosition, femaleAvatars[index].transform.rotation) as GameObject;
			}
			
			curAvatarDisplayedRef.transform.parent = this.gameObject.transform;
		}		
	}
	
	private void OnSelectionClick()
	{
		selectedIndex = index;
		
		// NOTE: 
		// Will need to do other GUI related actions to display 
		// to the user that they have selected their avatar to use
	}
	
	private void OnToggleGenderClick()
	{
		if(curAvatarDisplayedRef != null)
		{
			avatarPosition = new Vector3(this.transform.position.x, this.transform.position.y - 200.0f, this.transform.position.z - 25.0f);
			Destroy(curAvatarDisplayedRef);
			if(currentGenderState == AvatarGenderState.Female)
			{
				curAvatarDisplayedRef = Instantiate(maleAvatars[index], avatarPosition, maleAvatars[index].transform.rotation) as GameObject;
				
				currentGenderState = AvatarGenderState.Male;
			}
			else // We are male...
			{
				curAvatarDisplayedRef = Instantiate(femaleAvatars[index], avatarPosition, femaleAvatars[index].transform.rotation) as GameObject;
				
				currentGenderState = AvatarGenderState.Female;				
			}
			
			curAvatarDisplayedRef.transform.parent = this.gameObject.transform;
		}
	}
#endregion
}
