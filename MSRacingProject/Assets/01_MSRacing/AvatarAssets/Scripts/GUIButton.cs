using UnityEngine;
using System.Collections;

/// <summary>
/// GUI button, To use this class make sure you create a GUITexture, add this  
/// script to it and then add the textures that you wish to apply to your 
/// button in the public texture areas.
/// </summary>
public class GUIButton : MonoBehaviour 
{
	public Texture2D normalState;
	public Texture2D overState;
	public Texture2D activeState;
	
	public bool is3DObject = false;
	
	/// <summary>
	/// Button action handler.
	/// </summary>
	public delegate void ButtonActionHandler();
	/// <summary>
	/// Occurs when on button click is called.
	/// </summary>
	public event ButtonActionHandler OnButtonClick;
	
	private bool isActive = false;
	
	void Start()
	{
	}
	
	void OnMouseOver()
	{
		if(!isActive && !is3DObject)
			gameObject.guiTexture.texture = overState;
		else if(!isActive && is3DObject)
			gameObject.renderer.material.mainTexture = overState;
	}
	
	void OnMouseDown()
	{
		isActive = true;
		
		if(!is3DObject)
			gameObject.guiTexture.texture = activeState;
		else
			gameObject.renderer.material.mainTexture = activeState;
		
		if(OnButtonClick != null)
			OnButtonClick();
	}
	
	void OnMouseUp()
	{
		isActive = false;
	
		if(!is3DObject)
			gameObject.guiTexture.texture = normalState;	
		else
			gameObject.renderer.material.mainTexture = normalState;
	}
	
	void OnMouseExit()
	{
		if(!is3DObject)
			gameObject.guiTexture.texture = normalState;	
		else
			gameObject.renderer.material.mainTexture = normalState;
	}
}
